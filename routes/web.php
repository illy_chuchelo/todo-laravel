<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware'	=>	'auth', ], function(){

    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('/done', 'HomeController');
    Route::resource('/tasks', 'TasksController');
    Route::resource('/lists', 'ListsController');
});

Route::group(['middleware'	=>	'auth', 'namespace'=>'Auth'], function(){

    Route::get('/logout', 'AuthController@logout')->name('logout');

});

Route::group(['middleware'	=>	'guest', 'namespace'=>'Auth'], function(){

    Route::get('/register', 'AuthController@registerForm')->name('registerForm');
    Route::post('/register', 'AuthController@register')->name('register');

    Route::get('/verify/{token}', 'ConfirmationEmailController@confirm')->name('email.confirmation');

    Route::get('/email', 'ResetPassController@emailResetPass')->name('email.resetPassForm');
    Route::post('/email', 'ResetPassController@sendResetToken')->name('sendResetToken');

    Route::get('/resetPass/{token}/edit', 'ResetPassController@resetPassForm')->name('resetPassForm');
    Route::put('/resetPass/{token}', 'ResetPassController@resetPass')->name('resetPass');

    Route::group(['middleware'	=>	'checkstatus'], function(){

        Route::get('/login', 'AuthController@loginForm')->name('loginForm');
        Route::post('/login', 'AuthController@login')->name('login');

    });

});

Route::group(['middleware'	=>	'admin', 'namespace'=>'Admin'], function(){

    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::get('/userList', 'AdminController@usersList')->name('users.list');
    Route::put('/set_admin/{id}', 'AdminController@setAdmin');
    Route::put('/activate/{id}', 'AdminController@setStatus');

});