@extends('layout')

@section('content')
<div class="container">

    <h1>To-do</h1>

    @if(!empty($lists))

    <ul class="list-group">

        @foreach($lists as $list)


        <li class="list-group-item">{{$list->title}}
            <div class="btn-group pull-right">
                <a href="{{route('lists.edit', $list->id)}}" class="btn btn-outline-info">Edit</a>

                {{Form::open(['route'=>['lists.destroy', $list->id], 'method'=>'delete'])}}
                <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                {{Form::close()}}
            </div>
        </li>

            @foreach($list->getTask() as $task)

                <li class="list-group-item @if($task->done)  list-group-item-success @else list-group-item-primary @endif"> {{$task->name}}

                    <div class="btn-group pull-right">

                        {{Form::open(['action'=>['HomeController@update', $task->id], 'method'=>'put'])}}
                        <button onclick="" type="submit" class="btn btn-outline-success ">Mark</button>
                        {{Form::close()}}

                        <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-outline-info ">Edit</a>

                        {{Form::open(['route'=>['tasks.destroy', $task->id], 'method'=>'delete'])}}
                        <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-danger ">Delete</button>
                        {{Form::close()}}

                    </div>
                </li>
            @endforeach

            @include('tasks.create')

         @endforeach

    </ul>

@else

    <h1>Нет заданий</h1>

    @endif

    <a href="{{route('lists.create')}}" class="btn btn-success btn-lg btn-block">Создать новый списк</a>

</div>
@endsection