<!DOCTYPE html>
<html>

@include('head')

@include('errors.errors')

@if(session('verify'))
    <div class="alert alert-danger">
        {{session('verify')}}
    </div>
@endif
@if(session('verifyOk'))
    <div class="alert alert-primary">
        {{session('verifyOk')}}
    </div>
@endif

<body>

<div class="container">

    <div class="starter-template">

        @yield('content')

    </div>

</div>

</body>

</html>