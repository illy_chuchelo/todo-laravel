@extends('layout')

@section('content')
<h3>Создать новый список</h3><br/>

{{Form::open([
         'route'=> ['lists.store'],
         'method'=>'post'
         ])}}
<div class="form-group">

    <label for="title">Название</label>

    <input type="text" id="title" name="title" class="form-control" />

</div>

<div class="box-body">
    <div class="box-footer">
        <button class="btn btn-default">Назад</button>
        <button class="btn btn-warning pull-right">Создать</button>
    </div>
</div>

{{Form::close()}}

@endsection