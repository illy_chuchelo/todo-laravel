@extends('layout')

@section('content')

    <h3>Edit list</h3><br/>

    {{Form::open([
             'route'=> ['lists.update',$list->id],
             'method'=>'put'
             ])}}
    <div class="form-group">
        <label for="name">Список</label>
        <input type="text" id="title" name="title" value="{{$list->title}}" class="form-control" />
    </div>
    <button  class="btn btn-warning ">Изменить</button>

    {{Form::close()}}

@endsection