{{Form::open(['route'=> ['tasks.store'] ])}}

<div class="form-group">
    <input type="text" id="name" name="name" value="" class="form-control" placeholder="Введите Заметку"/>
    <input type="hidden" name = "list_id" id="list_id" value="{{$list->id}}">
    <button type="submit" class="btn btn-primary">Добавить</button>
</div>

{{Form::close()}}