@extends('layout')

@section('content')

    <h3>Edit task</h3><br/>

    {{Form::open([
             'route'=> ['tasks.update',$task->id],
             'method'=>'put'
             ])}}
        <div class="form-group">
            <label for="name">Задача</label>
            <input type="text" id="name" name="name" value="{{$task->name}}" class="form-control" />

        </div>
    <button class="btn btn-warning ">Изменить</button>


    {{Form::close()}}

@endsection