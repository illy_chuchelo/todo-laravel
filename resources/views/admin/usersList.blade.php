@extends('layout')

@section('content')
<div class="container">

    <table class="table">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Email</th>
            <th scope="col">Is admin</th>
            <th scope="col">Status</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
        <tr class=" @if($user->status) @else  table-secondary @endif">
            <th  scope="row">{{$user->id}}</th>
            <td>{{$user->email}}</td>
            <td>
                {{Form::open(['url'=>['/set_admin', $user->id], 'method'=>'put'])}}
                <button onclick="" type="submit" class="  @if($user->is_admin)  btn btn-success @else btn btn-outline-success @endif">
                    Админ
                </button>
                {{Form::close()}}

            </td>
            <td>
                {{Form::open(['url'=>['/activate', $user->id], 'method'=>'put'])}}
                <button onclick="" type="submit" class="btn btn-outline-info">Активировать</button>
                {{Form::close()}}
            </td>
        </tr>
        @endforeach
        </tbody>

    </table>

</div>
@endsection