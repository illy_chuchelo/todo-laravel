<head>


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>My ToDo list</title>
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{route('home')}}">To-Do</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">

            @if (Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="{{route('logout')}}">Logout</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('home')}}">{{Auth::user()->name}}</a>
                </li>

            @else

                <li class="nav-item">
                    <a class="nav-link" href="{{route('loginForm')}}">Login</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('registerForm')}}">Register</a>
                </li>
            @endif

                @if (Auth::check() && Auth::user()->is_admin)
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin')}}">All Lists</a>
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{route('users.list')}}">Users list</a>
            </li>
                @endif

        </ul>

    </div>
</nav>
