<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name'];//поля для заполнения

    //protected $appends = ['category'];

    public function lists()
    {
        return $this->belongsTo(Lists::class );
    }

    public static function add($fields)//добавление поста
    {
        $task = new static;
        $task->fill($fields);          //заполняет поля из $fillable
        //запись list_id не через форму
        $task->save();

        return $task;
    }

    public function edit($fields)      //изменить пост
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()           //удалить пост
    {
        $this->delete();
    }

    public function setList($id)
    {
        if($id == null){return;}

        $this->list_id = $id;
        $this->save();
    }

    public function mark()
    {
        $this->done = 1;
        $this->save();
    }

    public function unmark()
    {
        $this->done = 0;
        $this->save();
    }

    public function toggleStatus()
    {
        if( $this->done == 0)
        {
            return $this->mark();
        }
        return $this->unmark();
    }

}
