<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ResetPass;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResetPassController extends Controller
{

    public function emailResetPass()
    {
        return view('auth.email');
    }

    public function sendResetToken(Request $request)
    {
        $this->validate($request, [
            'email' =>'required|email',
        ]);

        $user = User::where('email', $request->get('email'))->first();
        if($user === null)
        {
            return redirect()->route('auth.email')->with('status', 'Не верный Email');
        }
        echo route('resetPassForm', ['reset_pass_token'=>$user->reset_pass_token]);
        \Mail::to($user)->send(new ResetPass($user));

        return redirect()->route('loginForm')->with('status', 'Запрос на обновление пароля отправленно на почту');

    }

    public function resetPassForm($token)
    {
        $user = User::where('reset_pass_token', $token )->first();

        return view('auth.resetPass', compact('user'));
    }

    public function resetPass(Request $request, $token)
    {
        $this->validate($request, [
            'password'	=>	'required',
            'password_confirmation' => 'required|same:password',
        ]);

        $user = User::where('reset_pass_token', $token )->first();
        $user->edit($request->all());
        $user->generatePassword($request->get('password'));
        $user->generateResetPassToken();

        return redirect()->route('loginForm')->with('status', 'Пароль изменен');
    }
}
