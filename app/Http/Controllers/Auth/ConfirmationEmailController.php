<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ConfirmationEmailController extends Controller
{

    public function confirm($token)
    {
        $user = User::where('verify_token', $token )->first();

        if($user === null)
        {
            abort(404);
        }
        $user->activate();

        return redirect()->route('loginForm')->with('verifyOk', 'Почта потверждена');
    }

}
