<?php

namespace App\Http\Controllers\Admin;

use App\Lists;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function index()
    {
        $lists = Lists::all();
        return view('admin.index',  compact( 'lists'));
    }

    public function usersList()
    {
        $users = User::all();
        return view('admin.usersList',  compact( 'users'));
    }

    public function setAdmin($id)
    {
        $user = User::find($id);
        $user->toggleAdmin();

        return redirect()->route('users.list');
    }

    public function setStatus($id)
    {
        $user = User::find($id);
        $user->toggleStatus();

        return redirect()->route('users.list');
    }


}
