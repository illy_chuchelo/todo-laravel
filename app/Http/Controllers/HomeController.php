<?php

namespace App\Http\Controllers;

use App\Lists;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {
        $lists = Lists::where('users_id',  Auth::id())->get();

        return view('index', compact( 'lists'));
    }

    public function update($id)
    {
        $task = Task::find($id);
        $task->toggleStatus();

        return redirect()->route('home');
    }




}
