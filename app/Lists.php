<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Lists extends Model
{
    protected $fillable = ['title'];

    public function tasks()
    {
        return $this->hasMany(Task::class, 'list_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function add($fields)//добавление поста
    {
        $list = new static;

        $list->fill($fields);          //заполняет поля из $fillable
        $list->users_id = Auth::user()->id;
        $list->save();

        return $list;
    }

    public function edit($fields)      //изменить пост
    {
        $this->fill($fields);
        $this->save();
    }

    public function setUser($id)
    {
        if($id == null){return;}

        $this->users_id = $id;
        $this->save();
    }

    public function remove()           //удалить пост
    {
        $this->delete();
    }

    public function getTask()
    {
        return  $this->tasks;
    }

}
