<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function lists()
    {
        return $this->hasMany(Lists::class, 'lists_id');
    }

    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function edit($hidden)
    {
        $this->fill($hidden);
        $this->save();
    }

    public function generatePassword($password)
    {
        if($password != null)
        {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function generateResetPassToken()
    {
         $this->reset_pass_token = md5(uniqid(rand(),1));
         $this->save();
    }
    public function generateVerifyToken()
    {
        $this->verify_token = md5(uniqid(rand(),1));
        $this->save();
    }

    public function makeAdmin()
    {
        $this->is_admin = 1;
        $this->save();
    }

    public function makeNormal()
    {
        $this->is_admin = 0;
        $this->save();
    }

    public function toggleAdmin()
    {
        if($this->is_admin == 0)
        {
            return $this->makeAdmin();
        }
        return $this->makeNormal();
    }

    public function activate()
    {
        $this->status = 1;
        $this->verify_token = null;
        $this->save();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->save();
    }

    public function toggleStatus()
    {
        if($this->status == 0)
        {
            return $this->activate();
        }
        return $this->deactivate();
    }


}
